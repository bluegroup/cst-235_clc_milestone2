package beans;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean (name="user")
@ViewScoped
public class User {
	
	public String userName;
	public String password;

	//Getters and setters are used to establish a username and password.
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	//login() method authenticate the user based on the parameters established in the if else statement.
	//Only the username uses the IgnoreCase for input while the password does not.
	//Within the faces-config.xml the return of either "success" or "failure" the application redirects 
	//the user to the appropriate .xhtml page.
	public String login() {
		if(userName.equalsIgnoreCase("Cameron") && password.equals("password")) {
			return "success";
		}
		else {
			return "failure";
		}
	}
}
