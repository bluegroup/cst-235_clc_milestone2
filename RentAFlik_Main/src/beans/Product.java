package beans;

public class Product {
	String title;
	String genre;
	String leadAct;
	String studio;
	String director;
	int length;
	int year;
	double price;
	
	public Product() {
		this.title = "";
		this.genre = "";
		this.leadAct = "";
		this.studio = "";
		this.director = "";
		this.length = 0;
		this.year = 0;
		this.price = 0;
		
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public String getGenre() {
		return genre;
	}	
	public void setLeadAct(String leadAct) {
		this.leadAct = leadAct;
	}
	
	public String getLeadAct() {
		return leadAct;
	}	

	public void setStudio(String studio) {
		this.studio = studio;
	}
	
	public String getStudio() {
		return studio;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	public String getDirector() {
		return director;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}
}
