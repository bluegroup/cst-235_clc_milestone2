package controllers;

//Imports needed for functionality.
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.RegisterBean;
import database.RegisterDao;

@SuppressWarnings("serial")
public class ServletRegistration extends HttpServlet {
	public ServletRegistration() {
	 }
	 
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // Transfers inputs to variables.
	 String nickname = request.getParameter("nickname");
	 String emailaddress = request.getParameter("emailaddress");
	 String password = request.getParameter("password");
	 String firstname = request.getParameter("firstname");
	 String lastname = request.getParameter("lastname");
	 String phone = request.getParameter("phone");
	 
	 RegisterBean registerBean = new RegisterBean();
	 // JavaBeans for similar data.
	 registerBean.setNickname(nickname);
	 registerBean.setEmailAddress(emailaddress);
	 registerBean.setPassword(password);
	 registerBean.setFirstName(firstname);
	 registerBean.setLastName(lastname);
	 registerBean.setPhone(phone);
	 
	 RegisterDao registerDao = new RegisterDao();
	 
	 // Registration's core logic for insertion of data into database from user.
	 String registerUser = registerDao.registerUser(registerBean);
	 
	 if(registerUser.equals("Success")) // Can display successful message on home page.
	 {
	 request.getRequestDispatcher("/MainForm.xhtml").forward(request, response);
	 }
	 else // If it fails however it will be noted.
	 {
	 request.setAttribute("errMessage", registerUser);
	 request.getRequestDispatcher("/Registration.jsp").forward(request, response);
	 }
	 }
}
